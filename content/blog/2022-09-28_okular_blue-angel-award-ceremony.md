---
date: 2022-09-28
title: "Blue Angel Award Ceremony At EnviroInfo Conference: Okular Officially Receives Eco-Label, First Ever For Software Product"
categories:  [Okular, Blauer Engel, Award Ceremony, EnviroInfo Conference, Sustainability]
author: Joseph P. De Veaugh-Geiss
summary: On Wednesday 28 September Okular celebrated becoming the first Blue Angel eco-certified software product together with the German Environment Agency ('Umweltbundesamt'), the Environmental Campus Birkenfeld ('Umwelt Campus Birkenfeld'), and the EnviroInfo conference researchers and attendees. [Updated on Friday, 7 October 2022 for Okular's celebration at Akademy 2022 with KDE Eco cake.]
SPDX-License-Identifier: CC-BY-4.0
authors:
- SPDX-FileCopyrightText: 2022 Joseph P. De Veaugh-Geiss <joseph@kde.org>
---

*Note: This post was updated on Friday, 7 October 2022. See Section ["Update: Celebrating At Akademy 2022 (1 October)"]({{< ref "#akademy" >}}).*

On Wednesday 28 September 2022 [Okular](https://okular.kde.org/), a Free and Open Source advanced document reader that allows you to read, sign, and annotate PDFs, ePubs, MarkDowns, and many other types of documents, was officially celebrated for becoming the first software product to receive the Blue Angel eco-label.

As announced in March (read more [here](https://eco.kde.org/blog/2022-03-16-press-release-okular-blue-angel/)), Okular has been recognized for sustainable software design as reflected in the recent Blue Angel award criteria for software eco-certification. Introduced in 1978, the Blue Angel is the world’s earliest eco-label and the official environmental label awarded by the German government. With this award, Okular is the first and currently only eco-certified computer program within the 30 organizations of the Global Ecolabelling Network, which represents over 50 countries.

{{< container class="text-center" >}}

![Joseph, Harri, and Alexander on stage at the Blue Angel award ceremony.](/blog/images/2022-09-28_kde-receiving-ecocertification.webp)

*KDE's Okular receiving the Blue Angel eco-certification from the German Environment Agency ('Umweltbundesamt'). From left to right: Mathias Bornschein from the German Environment Agency with Joseph P. De Veaugh-Geiss, Alexander Lohnau, and Harri Porten from the KDE community.*

{{< /container >}}


The award ceremony took place at the 36th edition of the [EnviroInfo conference](https://informatik2022.gi.de/en/enviroinfo-2022) in Hamburg, Germany. Three KDE community members -- Joseph P. De Veaugh-Geiss, Alexander Lohnau, and Harri Porten -- were on stage to receive the award on behalf of Okular and the KDE community.

EnviroInfo is an international and interdisciplinary conference with a focus on environmental issues in information and communication technologies. Researchers and attendees at the event are, quoting the website, "[c]ombining and shaping national and international activities in the field of applied informatics and environmental informatics in making the world a better place for living." The KDE community is honored to have celebrated Okular's achievement at the event together with the German Environment Agency ('Umweltbundesamt'), the Environmental Campus Birkenfeld ('Umwelt Campus Birkenfeld'), and the larger EnviroInfo community.

Congratulations to Okular and KDE for official recognition of the outstanding work you are doing!

## Update: Celebrating At Akademy 2022 (1 October) {#akademy}

On Saturday 1 October, immediately following the KDE Eco panel discussion, Akademy 2022 attendees gathered to celebrate Okular's achievement in the outdoor gardens at Universitat Politècnica de Catalunya.

It was a beautiful day. Even more beautiful was seeing [Okular contributors](https://invent.kde.org/graphics/okular/-/graphs/master) and the wider KDE community coming together, smiling, and enjoying the delicious, sugar-free KDE Eco cake made especially for the occasion.

See the [toot](https://mastodon.social/web/@BE4FOSS/109093579604779746) about the celebration and follow us at our [BE4FOSS Mastodon](https://mastodon.social/web/@BE4FOSS) account.

{{< container class="text-center" >}}

![KDE Eco cake](/blog/images/2022-10-01_akademy_kde-eco-cake.webp)

*Celebrating Okular and the Blue Angel with KDE Eco cake at Akademy 2022!*

{{< /container >}}
